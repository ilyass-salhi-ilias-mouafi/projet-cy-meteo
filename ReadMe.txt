Exécution:
Pour exécuter le programme, veuillez saisir sur votre terminal la commande ./script.sh puis veuillez saisir les bonnes options et arguments.
Vous pouvez aussi saisir des arguments optionnels.
Si vous renseignez un format invalide, un message d'erreur vous sera affiché.

Help:
En cas de problème, saisissez la commande ./script.sh --help qui vous affichera toutes les informations nécessaires.