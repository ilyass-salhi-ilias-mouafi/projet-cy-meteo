#!/bin/bash
 
# Initialisation des options 
Altitude=0
ExisteDate=0
Date1=0
Date2=0
Fichier=0
Humidite=0
Modetriage=0
Nomfichier=0
Pression=0
Modepression=0
Zone=0
Temperature=0
Modetemperature=0
Vent=0

#---------------------------------------------------------------------------------------------------------------------------------------------------
                                            #Vérification des paramètres entrés et filtrage du fichier
#---------------------------------------------------------------------------------------------------------------------------------------------------
while getopts ":f:t:p:whmFGSAOQd:-:" option
do
	case $option in
		#Fichier
		f)
			Nomfichier=$OPTARG
			if (( $Fichier != 0 ))
			then
				echo "Format Invalide : Veuillez ne choisir qu'un seul fichier"
 				exit 1 
			fi
			Fichier=1
			;;
		#Temperature
		t)
			Temperature=1
			case "$OPTARG" in
				1)
					if (( $Modetemperature != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'un seul mode de temperature."
 						exit 1 
					fi
					Modetemperature=1
					if (( $Zone != 0 && ExisteDate != 0 ))
					then
						#Filtrage(découpage) de la colonne numero 11 du fichier filtré DateFiltre.csv
						cat DateFiltre.csv | cut -d ';' -f11 > TemperatureFiltre.csv
					else
						#Filtrage(découpage) de la colonne numero 11 du fichier $Nomfichier
						cat $Nomfichier | cut -d ';' -f11 > TemperatureFiltre.csv
					fi
					;;
				2)
					if (( $Modetemperature != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'un seul mode de temperature."
 						exit 1 
					fi
					Modetemperature=2
					if (( $Zone != 0 && ExisteDate != 0 ))
					then
						#Filtrage(découpage) de la colonne numero 11 du fichier filtré DateFiltre.csv
						cat DateFiltre.csv | cut -d ';' -f11 > TemperatureFiltre.csv
					else
						#Filtrage(découpage) de la colonne numero 11 du fichier $Nomfichier
						cat $Nomfichier | cut -d ';' -f11 > TemperatureFiltre.csv
					fi
					;;
				3)
					if (( $Modetemperature != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'un seul mode de temperature."
 						exit 1 
					fi
					Modetemperature=3
					if (( $Zone != 0 && ExisteDate != 0 ))
					then
						#Filtrage(découpage) de la colonne numero 11 du fichier filtré DateFiltre.csv
						cat DateFiltre.csv | cut -d ';' -f11 > TemperatureFiltre.csv
					else
						#Filtrage(découpage) de la colonne numero 11 du fichier $Nomfichier
						cat $Nomfichier | cut -d ';' -f11 > TemperatureFiltre.csv
					fi
					;;
				#Mode inconnue
				*)
					echo "Format Invalide : Veuillez saisir un mode correct pour la température. ($OPTARG)"
					exit 1
					;;
			esac
			;;
		#Pression
		p)
			Pression=1
			case "$OPTARG" in
				1)
					if (( $Modepression != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'un seul mode de pression."
 						exit 1 
					fi
					Modepression=1
					if (( $Zone != 0 && ExisteDate != 0 ))
					then
						#Filtrage(découpage) de la colonne numero 7 du fichier filtré DateFiltre.csv
						cat DateFiltre.csv | cut -d ';' -f7 > PressionFiltre.csv
					else
						#Filtrage(découpage) de la colonne numero 7 du fichier $Nomfichier
						cat $Nomfichier | cut -d ';' -f7 > PressionFiltre.csv
					fi
					;;
				2)
					if (( $Modepression != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'un seul mode de pression."
 						exit 1 
					fi
					Modepression=2
					if (( $Zone != 0 && ExisteDate != 0 ))
					then
						#Filtrage(découpage) de la colonne numero 7 du fichier filtré DateFiltre.csv
						cat DateFiltre.csv | cut -d ';' -f7 > PressionFiltre.csv
					else
						#Filtrage(découpage) de la colonne numero 7 du fichier $Nomfichier
						cat $Nomfichier | cut -d ';' -f7 > PressionFiltre.csv
					fi
					;;
				3)
					if (( $Modepression != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'un seul mode de pression."
 						exit 1 
					fi
					Modepression=3
					if (( $Zone != 0 && ExisteDate != 0 ))
					then
						#Filtrage(découpage) de la colonne numero 7 du fichier filtré DateFiltre.csv
						cat DateFiltre.csv | cut -d ';' -f7 > PressionFiltre.csv
					else
						#Filtrage(découpage) de la colonne numero 7 du fichier $Nomfichier
						cat $Nomfichier | cut -d ';' -f7 > PressionFiltre.csv
					fi
					;;
				#Mode inconnue
				*)
					echo "Format Invalide : Veuillez saisir un mode correct pour la pression. ($OPTARG)"
					exit 1
					;;
			esac
			;;
		#Vent
		w)
			if (( $Vent != 0 ))
			then
				echo "Format Invalide : Veuillez ne choisir qu'une seule fois l'option vent."
 				exit 1 
			fi
			Vent=1
			if (( $Zone != 0 && ExisteDate != 0 ))
			then
				#Filtrage(découpage) des colonnes numero 4 et 5 du fichier filtré DateFiltre.csv
				cat DateFiltre.csv | cut -d ';' -f4,5 > VentFiltre.csv
			else
				#Filtrage(découpage) des colonnes numero 4 et 5 du fichier $Nomfichier
				cat $Nomfichier | cut -d ';' -f4,5 > VentFiltre.csv
			fi
			;;
		#Altitude
		h)
			if (( $Altitude != 0 ))
			then
				echo "Format Invalide : Veuillez ne choisir qu'une seule fois l'option altitude."
 				exit 1 
			fi
			Altitude=1
			if (( $Zone != 0 && ExisteDate != 0 ))
			then
				#Filtrage(découpage) de la colonne numero 14 du fichier filtré DateFiltre.csv
				cat DateFiltre.csv | cut -d ';' -f14 > AltitudeFiltre.csv
			else
				#Filtrage(découpage) de la colonne numero 14 du fichier $Nomfichier
				cat $Nomfichier | cut -d ';' -f14 > AltitudeFiltre.csv
			fi
			;;
		#Humidité
		m)
			if (( $Humidite != 0 ))
			then
				echo "Format Invalide : Veuillez ne choisir qu'une seule fois l'option Humidite."
 				exit 1 
			fi
			Humidite=1
			if (( $Zone != 0 && ExisteDate != 0 ))
			then
				#Filtrage(découpage) de la colonne numero 6 du fichier filtré DateFiltre.csv
				cat DateFiltre.csv | cut -d ';' -f6 > HumiditeFiltre.csv
			else
				#Filtrage(découpage) de la colonne numero 6 du fichier $Nomfichier
				cat $Nomfichier | cut -d ';' -f6 > HumiditeFiltre.csv
			fi
			
			;;
		#Date
		d)
			if (( $ExisteDate != 0 ))
			then
				echo "Format Invalide : Veuillez ne choisir qu'une seule fois l'option Date."
 				exit 1 
			fi
			ExisteDate=1
			
			Date1=${OPTARG:0:10}
			Date2=${OPTARG:11:10}

			# Vérifie que la date 1 est au format YYYY-MM-DD
			if [[ ! $Date1 =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
  				echo "Format Invalide : Veuillez saisir la date 1 selon le format YYYY-MM-DD"
  				exit 1
			fi

			# Stock l'année, le mois et le jour dans des variables
			Annee1=${Date1:0:4}
			Mois1=${Date1:5:2}
			Jour1=${Date1:8:2}

			# Vérifie que l'année est valide
			if (( $Annee1 < 2000 || $Annee1 > 2100 )); then
  				echo "Format Invalide : Année date 1 invalide"
  				exit 1
			fi

			# Vérifie que le mois est valide
			if (( $Mois1 < 1 || $Mois1 > 12 )); then
  				echo "Format Invalide : Mois date 1 invalide"
  				exit 1
			fi

			# Vérifie que le jour est valide
			if (( $Jour1 < 1 || $Jour1 > 31 )); then
  				echo "Format Invalide : Jour date 1 invalide"
  				exit 1
  			fi

  			# Vérifie que la date 2 est au format YYYY-MM-DD
   			if [[ ! $Date2 =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
  				echo "Format Invalide : Veuillez saisir la date 2 selon le format YYYY-MM-DD"
  				exit 1
			fi

			# Stock l'année, le mois et le jour dans des variables
			Annee2=${Date2:0:4}
			Mois2=${Date2:5:2}
			Jour2=${Date2:8:2}

			# Vérifie que l'année est valide
			if (( $Annee2 < 2000 || $Annee2 > 2100 )); then
 				echo "Format Invalide : Année date 2 invalide"
  				exit 1
			fi

			# Vérifie que le mois est valide
			if (( $Mois2 < 1 || $Mois2 > 12 )); then
  				echo "Format Invalide : Mois date 2 invalide"
  				exit 1
			fi

			# Vérifie que le jour est valide
			if (( $Jour2 < 1 || $Jour2 > 31 )); then
  				echo "Format Invalide : Jour date 2 invalide"
  				exit 1
			fi

			# Vérifie si date1 plus petite que date2
			if (( $Annee1 > $Annee2 ))
			then
				echo "Format Invalide : Année Date1 plus grande que Année Date2"
				exit 1
			else 
				if (( $Annee1 == $Annee2 ))
				then
					if (( $Mois1 > $Mois2 ))
					then
						echo "Format Invalide : Mois Date1 plus grand que Mois Date2"
						exit 1
					else
						if (( $Mois1 == $Mois2 ))
						then
							if (( $Jour1 > $Jour2 ))
							then
								echo "Format Invalide : Jour Date1 plus grand que Jour Date2"
								exit 1
							fi
						fi
					fi
				fi
			fi
			#Filtrage par zones ET dates
			if (( $Zone != 0 ))
			then
				cat ZonesFiltre.csv | awk -F ";" -v date1=$Date1 -v date2=$Date2 '{if(date1 <= $2 && date2 >= $2)print $0}'> DateFiltre.csv
			#Filtrage QUE par dates
			else
				cat $Nomfichier | awk -F ";" -v date1=$Date1 -v date2=$Date2 '{if(date1 <= $2 && date2 >= $2)print $0}'> DateFiltre.csv
			fi
			;;
		-)
			case "$OPTARG" in
				avl)
					if (( $Modetriage != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'une seule option de tri."
 						exit 1 
					fi
					Modetriage=1
					;;
				abr)
					if (( $Modetriage != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'une seule option de tri."
 						exit 1 
					fi
					Modetriage=2
					;;
				tab)
					if (( $Modetriage != 0 ))
					then
						echo "Format Invalide : Veuillez ne choisir qu'une seule option de tri."
 						exit 1 
					fi
					Modetriage=3
					;;
				help)
					echo "Aide :"
					echo "Syntaxe: ./script.sh -f [nomfichier] [options] [typesDonnées] "
   					echo "Options:"		
   					echo "		-F       filtre les données choisies pour la France métropolitaine et la Corse."
   					echo "          -G       filtre les données choisies pour la Guyane."
   					echo "          -S       filtre les données choisies pour l’ile Saint-Pierre et Miquelon."
   					echo "          -A       filtre les données choisies pour les Antilles."
   					echo "          -O       filtre les données choisies pour l’Océan Indien."
  	 				echo "          -Q       filtre les données choisies pour l'Antarctique."
  	 				echo ""
   					echo "Types de Données:" 
   					echo "		-d <min> <max>  filtre les données choisies entre les dates min et max (modèle: YYYY-MM-DD/YYYY-MM-DD)."
   					echo "		-w	 	permet de choisir l’orientation moyenne et la vitesse moyenne des vents pour chaque station."
   					echo "		-h		permet de choisir l'altitude de chaque station."
   					echo "		-m 		permet de choisir l'humidité maximale pour chaque station."
   					echo "		-t<mode>	permet de choisir la température."
   					echo "		-p<mode>	permet de choisir la presion."
   					echo "Modes:"
   					echo "		1 	température/pression moyenne, minimale et maximale par station."
   					echo "		2 	température/pression moyenne pour toutes les stations par ordre chronologique."
   					echo "		3 	température/pression moyenne par station."
					exit 0
					;;
				*)
					echo "Format Invalide : option non reconnue. ($OPTARG)"
					exit 1
					;;
			esac
			;;
		# France métropolitaine et Corse
		F)
			if (( $Zone != 0 ))
			then
				echo "Format Invalide : Veuillez choisir un seul lieu à la fois."
 				exit 1 
			fi
			Zone=1
			#Filtre les valeurs de latitude entre 41 et 51 et longitude -5 et 8
			tail -n +2 $Nomfichier | grep ";[4-5][0-9]\.[0-9]*,-*[0-9]\." > ZonesFiltre.csv
			cat ZonesFiltre.csv > DateFiltre.csv
			;;
		# Guyane		
		G)
			if (( $Zone != 0 ))
			then
				echo "Format Invalide : Veuillez choisir un seul lieu à la fois."
 				exit 1 
			fi
			Zone=2
			#Filtre les valeurs de latitude entre 2 et 5.9 et longitude -54.65 et -51.5
			tail -n +2 $Nomfichier | grep ";[0-9]\.[0-9]*,-5[0-9]" > ZonesFiltre.csv
			cat ZonesFiltre.csv > DateFiltre.csv
			;;
		# Saint-Pierre et Miquelon
		S)
			if (( $Zone != 0 ))
			then
				echo "Format Invalide : Veuillez choisir un seul lieu à la fois."
 				exit 1 
			fi
			Zone=3
			#Filtre les valeurs de latitude entre 46.7 et 47.2 et longitude -56.4 et-56,1
			tail -n +2 $Nomfichier | grep ";[4-6][0-9]\.[0-9]*,-[4-6][0-9]" > ZonesFiltre.csv
			cat ZonesFiltre.csv > DateFiltre.csv
			;;
		# Antilles
		A)
			if (( $Zone != 0 ))
			then
				echo "Format Invalide : Veuillez choisir un seul lieu à la fois."
 				exit 1 
			fi
			Zone=4
			#Filtre les valeurs de latitude entre 15 et 26 et longitude entre -84 et-63
			tail -n +2 $Nomfichier | grep ";[1-3][0-9]\.[0-9]*,-[6-9][0-9]" > ZonesFiltre.csv
			cat ZonesFiltre.csv > DateFiltre.csv
			;;
		# Océan Indien
		O)
			if (( $Zone != 0 ))
			then
				echo "Format Invalide : Veuillez choisir un seul lieu à la fois."
 				exit 1 
			fi
			Zone=5
			#Filtre les valeurs de latitude entre -56 et-5 et longitude entre 35 et 112
			tail -n +2 $Nomfichier | grep ";-[1-6][0-9]\.[0-9]*,[4-9][0-9]" > ZonesFiltre.csv
			cat ZonesFiltre.csv > DateFiltre.csv
			;;
		# Antarctique
		Q)
			if (( $Zone != 0 ))
			then
				echo "Format Invalide : Veuillez choisir un seul lieu à la fois."
 				exit 1 
			fi
			Zone=6
			#Filtre les valeurs de latitude entre -90 et-60 et longitude on prend toutes les valeurs
			tail -n +2 $Nomfichier | grep ";-[6-9][0-9]\.[0-9]*," > ZonesFiltre.csv
			cat ZonesFiltre.csv > DateFiltre.csv
			;;
		# Option inconnue
		?)
			echo "Format Invalide : option non reconnue. ($OPTARG)"
			exit 1
			;;
	esac
done

# Vérifie si le fichier est entré (option obligatoire)
if (( $Fichier == 0 ))
then
	echo "Format Invalide : Veuillez saisir un fichier avec l'option -f."
	exit 1
fi

# Vérifie qu'au moins un type de données est entré (option obligatoire)
if (( $Temperature == 0 )) && (( $Humidite == 0 )) && (( $Altitude == 0 )) && (( $Vent == 0 )) && (( $Pression == 0 ))
then
	echo "Format invalide : Veuillez saisir au moins un type de données (Pression, Température, Humidite, Altitude, Vent)."
	exit 1
fi 

# Sélectionne tri avl par défaut
if (( $Modetriage == 0 ))
then
	Modetriage=1
fi

exit 0
