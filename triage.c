#include<stdio.h>
#include<stdlib.h>

//---------ABR---------
typedef struct arbre{
	int elm;
	struct arbre* fg;
	struct arbre* fd;
}Arbre;

Arbre* creerArbre(int r){
	Arbre* pNode=malloc(sizeof(Arbre));
	pNode->elm=r;
	pNode->fg=NULL;
	pNode->fd=NULL;
	return pNode;
}

void ajouteFG(Arbre* a, int r){
	if(a==NULL){
		a=creerArbre(r);
	}
	else if(existeFG(a)==0){
		a->fg=creerArbre(r);
	}
}

void ajouteFD(Arbre* a, int r){
	if(a==NULL){
		a=creerArbre(r);
	}
		else if(existeFD(a)==0){
	a->fd=creerArbre(r);
	}
}

Arbre* insertABR(Arbre* a, int e){
	if(a==NULL)
		return creerArbre(e);
	else if(e< a->elm)
		a->fg=insertABR(a->fg,e);
	else if(e> a->elm)
		a->fd=insertABR(a->fd,e);
	return a;
}

Arbre* insertABRbis(Arbre* a, int e){
	if(a==NULL){
		a=creerArbre(e);
		return a;
	}
	Arbre* pA=a;
	while(pA!=NULL){
		if(e < pA->elm){
			if(pA->fg!=NULL){
				pA=pA->fg;
			}
			else{
				pA->fg=creerArbre(e);
				pA=NULL;
			}	
		}
		else if(e> pA->elm){
			if(pA->fd!=NULL){
				pA=pA->fd;
			}
			else{
				pA->fd=creerArbre(e);
				pA=NULL;
			}	
		}
		else{
			pA=NULL;
		}
		
	}
	return a;
	
}
//---------AVL---------
typedef struct arbre{
	int elm;
	char* date;
	struct arbre* fg;
	struct arbre* fd;
	int equilibre;
}Arbre;

Arbre* creerArbre(int r){
	Arbre* pNode=malloc(sizeof(Arbre));
	pNode->elm=r;
	pNode->fg=NULL;
	pNode->fd=NULL;
	pNode->equilibre=0;
	return pNode;
}

Arbre* insertAVL(Arbre* a, int e, int* h){
	if(a==NULL){
		*h=1;
		return creerArbre(e);
	}
	else if(e< a->elm){
		a->fg=insertAVL(a->fg,e);
		*h=-*h;
	}
	else if(e> a->elm){
		a->fd=insertAVL(a->fd,e);
	}
	else{
		*h=0;
		return a;
	}
	if(*h!=0){
		a->equilibre += *h;
		if(a->equilibre==0){
			*h=0;
		}
		else
			*h=1;
	}
	return a;
}

int max(int x, int y){
	if(x >= y){	
		return x;}
	return y;
}

int max(int x, int y){
	if(x >= y){	
		return y;}
	return x;
}


Arbre* rotationG(Arbre* a){
	Arbre* pivot;
	int eq_a ,eq_p;
	pivot=a->fd;
	a->fd=pivot->fg;
	pivot->fg=a;
	eq_a=a->equilibre;
	eq_p = pivot-> equilibre;
	a-> equilibre= eq_a - max(eq_p,0) -1;
	pivot -> equilibre = min (eq_a-2, eq_a+ eq_p-2,eq_p-1);
	a=pivot;
	return a;
}

Arbre* rotationD(Arbre*a){	
	Arbre* pivot;
	int eq_a ,eq_p;
	pivot=a->fg;
	a->fg=pivot->fd;
	pivot->fd=a;
	eq_a=a->equilibre;
	eq_p = pivot-> equilibre; 
	a-> equilibre= eq_a - min(eq_p,0) +1;
	pivot -> equilibre = max(eq_a+2, eq_a+ eq_p+2,eq_p+1);
	a=pivot;
	return a;
}

//---------Liste chainée---------
typedef struct chainon{
	int elm;
	struct chainon* pNext;
}Chainon;

typedef struct file{
	Chainon* pHead;
	Chainon* pTail;
}File;

Chainon* creerChainon(int a){
	Chainon* pNode=malloc(sizeof(Chainon));
	if(pNode==NULL){
		exit(1);
	}
	pNode->elm=a;
	pNode->pNext=NULL;
	return pNode;
}

Chainon* insertFin(Chainon* pHead, int a){
	Chainon* pNew=creerChainon(a);
	if(pNew==NULL){
		exit(3);
	}
	Chainon* pNode=pHead;
	if(pHead==NULL){
		pHead=pNew;
	}
	else{
		while(pNode->pNext != NULL){
		pNode=pNode->pNext;
		}
		pNode->pNext=pNew;
	}
	return pHead;
}

File* creerFile(){
	File* pFile=malloc(sizeof(File));
	if(pFile==NULL){
		exit(99);
	}
	pFile->pHead=NULL;
	pFile->pTail=NULL;
	return pFile;
}

void enfiler(File* pFile, Arbre* a){
	Chainon* pNew=creerChainon(a);
	if(pFile->pHead==NULL && pFile->pTail==NULL){
		pFile->pHead=pNew;
		pFile->pTail=pNew;
	}
	else{
		pFile->pTail->pNext=pNew;
		pFile->pTail=pNew;
	}
int main(int argc, char const *argv[]){
	return 0;
}